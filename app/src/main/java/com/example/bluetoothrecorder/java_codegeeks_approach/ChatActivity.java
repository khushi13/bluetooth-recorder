package com.example.bluetoothrecorder.java_codegeeks_approach;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.bluetoothrecorder.BaseActivity;
import com.example.bluetoothrecorder.R;
import com.example.bluetoothrecorder.recording.RecorderActivity;
import com.example.bluetoothrecorder.java_codegeeks_approach.recyclerView.MessageAdapter;
import com.example.bluetoothrecorder.java_codegeeks_approach.recyclerView.MessageModel;
import com.example.bluetoothrecorder.java_codegeeks_approach.utils.BluetoothChatService;
import com.example.bluetoothrecorder.java_codegeeks_approach.utils.ICommand;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.io.FileUtils;

public class ChatActivity extends BaseActivity {

  private boolean isRecording = false;
  private boolean isTransferVideo = false;

  // MessageModel types sent from the BluetoothChatService Handler
  public static final int MESSAGE_STATE_CHANGE = 1;
  public static final int MESSAGE_READ = 2;
  public static final int MESSAGE_WRITE = 3;
  public static final int MESSAGE_DEVICE_NAME = 4;
  public static final int MESSAGE_TOAST = 5;


  // Key names received from the BluetoothChatService Handler
  public static final String DEVICE_NAME = "device_name";
  public static final String TOAST = "toast";

  // Intent request codes
  private static final int REQUEST_CONNECT_DEVICE = 1;
  private static final int REQUEST_ENABLE_BT = 2;
  private EditText mOutEditText;
  private Button mSendButton;
  private ProgressBar progress;

  // Name of the connected device
  private String mConnectedDeviceName = null;
  // String buffer for outgoing messages
  private StringBuffer mOutStringBuffer;

  // Local Bluetooth adapter
  private BluetoothAdapter mBluetoothAdapter = null;

  // Member object for the chat services
  private BluetoothChatService mChatService = null;

  private RecyclerView mRecyclerView;
  private LinearLayoutManager mLayoutManager;
  private MessageAdapter mAdapter;

  public int counter = 0;

  private List messageList = new ArrayList();
  private final int REQUEST_START_RECORDING = 102;

  @Override
  public String className() {
    return ChatActivity.class.getSimpleName();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chat);

    debugLog("onCreate()--->");
    mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
    mRecyclerView.setHasFixedSize(true);
    mLayoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mAdapter = new MessageAdapter(getBaseContext(), messageList);
    mRecyclerView.setAdapter(mAdapter);
    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    // If the adapter is null, then Bluetooth is not supported
    if (mBluetoothAdapter == null) {
      Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
      finish();
      return;
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    debugLog("onStart()--->");
    if (!mBluetoothAdapter.isEnabled()) {
      Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
    } else {
      if (mChatService == null) setupChat();
    }
  }

  @Override
  public synchronized void onResume() {
    super.onResume();
    debugLog("onResume()--->");
    if (mBluetoothAdapter.getScanMode() !=
        BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
      debugLog("no discoverable in onResume()");
    else
      debugLog("BT discoverable");

    if (mChatService != null) {
      if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
        debugLog("chat service not null and starting");
        mChatService.start();
      }
    } else {
      debugLog("chat service null");
    }
  }

  private void setupChat() {
    mOutEditText = (EditText) findViewById(R.id.edit_text_out);
    mOutEditText.setOnEditorActionListener(mWriteListener);
    mSendButton = (Button) findViewById(R.id.button_send);
    mSendButton.setOnClickListener(v -> {
      TextView view = (TextView) findViewById(R.id.edit_text_out);
      String message = view.getText().toString();
      sendMessage(message);
    });
    ((Button) findViewById(R.id.start_rec_btn)).setOnClickListener(v -> {
      if (!isRecording)
      sendMessage(ICommand.START_RECORDING);
    });
    ((Button) findViewById(R.id.stop_rec_btn)).setOnClickListener(v -> {
      if (isRecording)
      sendMessage(ICommand.STOP_RECORDING);
    });
    progress = (ProgressBar) findViewById(R.id.progress);

    // Initialize the BluetoothChatService to perform bluetooth connections
    mChatService = new BluetoothChatService(this, mHandler);

    // Initialize the buffer for outgoing messages
    mOutStringBuffer = new StringBuffer("");
  }

  @Override
  public synchronized void onPause() {
    super.onPause();
    debugLog("onPause()--->");

  }

  @Override
  public void onStop() {
    super.onStop();
    debugLog("onStop()--->");

  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    // Stop the Bluetooth chat services
    debugLog("onDestroy()--->");
    if (mChatService != null) mChatService.stop();
  }

  private void ensureDiscoverable() {
    debugLog("ensureDiscoverable()");
    if (mBluetoothAdapter.getScanMode() !=
        BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
      Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
      discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
      startActivity(discoverableIntent);
    } else {
      debugLog("already discoverable");
    }
  }


  private void sendMessage(String message) {

    isTransferVideo = false;

    // Check that we're actually connected before trying anything
    if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
      Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
      return;
    }
    // Check that there's actually something to send
    if (message.length() > 0) {
      // Get the message bytes and tell the BluetoothChatService to write
      byte[] send = message.getBytes();
      mChatService.write(send);
      // Reset out string buffer to zero and clear the edit text field
      mOutStringBuffer.setLength(0);
      mOutEditText.setText(mOutStringBuffer);
    }
  }

  private void sendVideo(Uri videoUri) {

    isTransferVideo = true;

    debugLog("sendVideo videoUri: " +videoUri.getPath());
    InputStream iStream = null;
    byte[] inputData = null;

    try {
      //iStream = getContentResolver().openInputStream(videoUri);
      /*
      if (iStream != null) {
        inputData= getBytesFromInputStream(iStream);
        debugLog("inputData byte[] :" + inputData);
      }
      */
      inputData = getByteArrayFromFile(videoUri);
    } catch (IOException e) {
      debugLog("send video exception:" + e.getMessage());
      e.printStackTrace();
    }

    if (inputData != null) {
      mChatService.write(inputData);
      //mOutStringBuffer.setLength(0);
      String outString = new String(inputData);
      debugLog("send video string from byte [] : " + outString);
    }
  }

  // The action listener for the EditText widget, to listen for the return key
  private TextView.OnEditorActionListener mWriteListener = (view, actionId, event) -> {
    // If the action is a key-up event on the return key, send the message
    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
      String message = view.getText().toString();
      sendMessage(message);
    }
    return true;
  };

  // The Handler that gets information back from the BluetoothChatService
  @SuppressLint("HandlerLeak")
  private final Handler mHandler = new Handler() {
    @Override
    public void handleMessage(Message msg) {
      switch (msg.what) {
        case MESSAGE_WRITE:
          debugLog("isTransferVideo : " +isTransferVideo);
          byte[] writeBuf = (byte[]) msg.obj;
          if (!isTransferVideo) {
            // construct a string from the buffer
            String writeMessage = new String(writeBuf);
            debugLog("message write:" + writeMessage);
            if (ICommand.START_RECORDING.equals(writeMessage)) {
              debugLog("start recording");
              isRecording = true;
              isTransferVideo = false;
              hideKeyboard(ChatActivity.this);
            } else if (ICommand.STOP_RECORDING.equals(writeMessage)) {
              debugLog("stop recording");
              isRecording = false;
              isTransferVideo = true;
              showProgress();
              hideKeyboard(ChatActivity.this);
            } else {
              mAdapter.notifyDataSetChanged();
              messageList.add(new MessageModel(counter++, writeMessage, "Me"));
            }
          } else {
            debugLog("start video transfer");
            showProgress();
          }
          break;
        case MESSAGE_READ:
          byte[] readBuf = (byte[]) msg.obj;
          // construct a string from the valid bytes in the buffer

          if (isTransferVideo) {
            //String readMessage = new String(readBuf, "UTF-8");
            debugLog("start video reading");
            debugLog("message read:" + readBuf.length);
            //Uri videoUri = Uri.parse(readMessage);
            //debugLog("read transfer video : " + videoUri.toString());
            FileOutputStream out = null;
            try {
              out = getOutputStreamFromBytes(readBuf);
            } catch (IOException e) {
              e.printStackTrace();
            }

            hideProgress();
          } else {
            String readMessage = new String(readBuf, 0, msg.arg1);
            debugLog("message read:" + readMessage);
            if (ICommand.START_RECORDING.equals(readMessage)) {
              hideKeyboard(ChatActivity.this);
              startActivityForResult(new Intent(ChatActivity.this, RecorderActivity.class),
                  REQUEST_START_RECORDING);
            } else if (ICommand.STOP_RECORDING.equals(readMessage)) {
              RecorderActivity.stopVideoRecording();
            } else {
              mAdapter.notifyDataSetChanged();
              messageList.add(new MessageModel(counter++, readMessage, mConnectedDeviceName));
            }
          }
          break;
        case MESSAGE_DEVICE_NAME:
          // save the connected device's name
          mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
          Toast.makeText(getApplicationContext(), "Connected to "
              + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
          break;
        case MESSAGE_TOAST:
          Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
              Toast.LENGTH_SHORT).show();
          break;
      }
    }
  };

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    debugLog("onActivityResult()");
    switch (requestCode) {
      case REQUEST_CONNECT_DEVICE:
        // When DeviceListActivity returns with a device to connect
        if (resultCode == Activity.RESULT_OK) {
          // Get the device MAC address
          String address = data.getExtras().getString(ListActivity.EXTRA_DEVICE_ADDRESS);
          // Get the BLuetoothDevice object
          BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
          // Attempt to connect to the device
          mChatService.connect(device);
        }
        break;
      case REQUEST_ENABLE_BT:
        // When the request to enable Bluetooth returns
        if (resultCode == Activity.RESULT_OK) {
          // Bluetooth is now enabled, so set up a chat session
          setupChat();
        } else {
          // User did not enable Bluetooth or an error occured
          Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
          finish();
        }
        break;
      case REQUEST_START_RECORDING:
        if (resultCode == Activity.RESULT_OK ) {
          if (data != null && data.getData() != null) {
            Uri videoUri = data.getData();
            debugLog("output recorded video:" + videoUri.getPath());
            mOutEditText.setText(videoUri.getPath());
            showProgress();
            sendVideo(videoUri);
          }
          debugLog("video recording success.");
        }
        break;
    }
  }

  public void connect(View v) {
    debugLog("connect()");
    Intent serverIntent = new Intent(this, ListActivity.class);
    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
  }

  public void discoverable(View v) {
    ensureDiscoverable();
  }


  public byte[] getBytesFromInputStream(InputStream inputStream) throws
      IOException {
    debugLog("get bytes from video");
    ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
    int bufferSize = 1024;
    byte[] buffer = new byte[bufferSize];

    int len = 0;
    while ((len = inputStream.read(buffer)) != -1) {
      debugLog("get bytes len:" + len);
      byteBuffer.write(buffer, 0, len);
    }
    return byteBuffer.toByteArray();
  }

  public FileOutputStream getOutputStreamFromBytes(byte[] byteArray) throws
      IOException{

    FileOutputStream out = new FileOutputStream(Environment
        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() +
        "/BT_video.mp4");
    out.write(byteArray);
    out.close();
    return out;
  }

  private void showProgress() {
    progress.setVisibility(View.VISIBLE);
  }

  private void hideProgress() {
    progress.setVisibility(View.GONE);
  }

  public static void hideKeyboard(Activity activity) {
    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
    //Find the currently focused view, so we can grab the correct window token from it.
    View view = activity.getCurrentFocus();
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
      view = new View(activity);
    }
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

  private byte[] getByteArrayFromFile(Uri fileUri) throws
      IOException {
    File file = new File(Objects.requireNonNull(fileUri.getPath()));

    return FileUtils.readFileToByteArray(file);
  }

  private String getStringFromByteArray(byte[] array) throws
      IOException {
    return Base64.encodeToString(array, Base64.NO_WRAP);
  }
}
