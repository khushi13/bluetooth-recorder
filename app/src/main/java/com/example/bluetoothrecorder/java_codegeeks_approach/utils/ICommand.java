package com.example.bluetoothrecorder.java_codegeeks_approach.utils;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({ ICommand.START_RECORDING, ICommand.STOP_RECORDING})
@Retention(RetentionPolicy.SOURCE)
public @interface ICommand {

  String START_RECORDING = "START_RECORDING";

  String STOP_RECORDING = "STOP_RECORDING";

}
