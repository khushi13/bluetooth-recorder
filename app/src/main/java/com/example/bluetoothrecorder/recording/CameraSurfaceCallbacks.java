package com.example.bluetoothrecorder.recording;

import android.graphics.SurfaceTexture;

public interface CameraSurfaceCallbacks {

  void initializeCameraOnView(SurfaceTexture surfaceTexture);

  void releaseCameraFromView();
}
