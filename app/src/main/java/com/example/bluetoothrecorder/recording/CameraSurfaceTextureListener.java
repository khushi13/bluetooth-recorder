package com.example.bluetoothrecorder.recording;

import android.graphics.SurfaceTexture;
import android.view.TextureView;

public class CameraSurfaceTextureListener implements TextureView.SurfaceTextureListener {

  CameraSurfaceCallbacks mCallbacks;

  public CameraSurfaceTextureListener(CameraSurfaceCallbacks callbacks) {
    this.mCallbacks = callbacks;
  }

  @Override
  public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
    mCallbacks.initializeCameraOnView(surface);
  }

  @Override
  public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

  }

  @Override
  public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
    mCallbacks.releaseCameraFromView();
    return true;
  }

  @Override
  public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    //mCallbacks.updateCameraOnView(surface);
  }
}
