package com.example.bluetoothrecorder.recording;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.view.TextureView;
import android.widget.Toast;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import com.example.bluetoothrecorder.BaseActivity;
import com.example.bluetoothrecorder.R;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecorderActivity extends BaseActivity implements CameraSurfaceCallbacks {

  public boolean isRecording = false;

  private int mProgressValue = 0;
  private Handler mHandler;
  private Runnable mRunnable;
  private static final int INTERVAL_IN_MILLISECONDS = 1000;
  private final int COUNTDOWN_TIMER_INTERVAL = 500;
  private static final int VIDEO_TIME_DURATION_IN_SECONDS = 7;
  private CountDownTimer countDownTimer;
  private static final int MAX_VIDEO_MESSAGE_DURATION_MILLISECONDS =
      VIDEO_TIME_DURATION_IN_SECONDS * INTERVAL_IN_MILLISECONDS;

  private static final int COUNT_DOWN_TIME = 3000;
  private int recordingDuration;

  private CameraSurfaceTextureListener mSurfaceTextureListener;
  private static Camera mCamera;
  private CamcorderProfile mCamcorderProfile;
  private static MediaRecorder mVideoRecorder;
  private static boolean isRecordingVideo, isVideoRecorderPrepared;
  private static File mOutputFile;

  public static Activity activity;

  private String[] permissions = {
      Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO,
      Manifest.permission.CAMERA
  };
  private final int PERMISSION_ALL = 101;
  private TextureView surfaceView;

  public void stopRecording() {
    RecorderActivity.this.finish();
  }

  @Override
  public String className() {
    return RecorderActivity.class.getSimpleName();
  }

  @Override
  public void onResume() {
    super.onResume();
    debugLog("Entering onResume()>>>>>");
    // requestAllCameraRelatedPermissions();
    surfaceView.setAlpha(1.0F);
    /*
    if (mPresenter != null && fileName == null && !isRecording) {
      mPresenter.getCredentialsToUploadVideo(Constants.TEMP_FILE_NAME,
          isKindnessNotificationReceived ? kindnessModel.getReceiverInformationModel()
              .getReceiverId() : kindnessModel.getReceiverInformationModel().getReceiverNumber());
    }
    */
    if (mCamera == null && surfaceView.isAvailable() && !isRecordingVideo) {
      this.initializeCameraOnView(surfaceView.getSurfaceTexture());
    } else {
      surfaceView.setSurfaceTextureListener(mSurfaceTextureListener);
    }
    debugLog("Exiting onResume()>>>>>");
  }

  @Override
  public void onPause() {
    super.onPause();
    debugLog("Entering onPause()>>>>>>");
    if (isRecordingVideo || isVideoRecorderPrepared) {
      try {
        mVideoRecorder.stop();
      } catch (RuntimeException e) {
        debugLog("Exception catched while recorder stop in onPause() -> " + e.getMessage());
      }
      isRecordingVideo = false;
      isVideoRecorderPrepared = false;
      releaseRecorder();
    }
    debugLog("Exiting onPause()>>>>>");
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    releaseRecorder();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recorder);
    surfaceView = (TextureView) findViewById(R.id.surface_view);

    activity = RecorderActivity.this;

    if(!hasPermissions(this, permissions)){
      ActivityCompat.requestPermissions(this, permissions, PERMISSION_ALL);
    } else {
      initiateView();
    }
  }

  private void initiateView() {
    mSurfaceTextureListener = new CameraSurfaceTextureListener(this);
    surfaceView.setSurfaceTextureListener(mSurfaceTextureListener);

    if (surfaceView.isAvailable()) {
      mSurfaceTextureListener.onSurfaceTextureAvailable(
          surfaceView.getSurfaceTexture(), surfaceView.getWidth(),
          surfaceView.getHeight());
    }
  }

  private void initializeCameraView(Camera camera, SurfaceTexture surfaceTexture) {
    debugLog("Entering initializeCameraView()");
    mCamera = camera;
    Camera.Parameters parameters = mCamera.getParameters();
    List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
    List<Camera.Size> mSupportedVideoSizes = parameters.getSupportedVideoSizes();
    Camera.Size optimalSize =
        getOptimalVideoSize(mSupportedVideoSizes, mSupportedPreviewSizes,
            surfaceView.getWidth(), surfaceView.getHeight());
    mCamcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
    if (optimalSize != null) {

      mCamcorderProfile.videoFrameWidth = optimalSize.width;
      mCamcorderProfile.videoFrameHeight = optimalSize.height;
    } else {

      int width = mCamera.getParameters().getPreviewSize().width;
      int height = mCamera.getParameters().getPreviewSize().height;
      mCamcorderProfile.videoFrameWidth = width;
      mCamcorderProfile.videoFrameHeight = height;
    }
    mCamcorderProfile.videoFrameRate = 30;
    parameters.setPreviewSize(mCamcorderProfile.videoFrameWidth,
        mCamcorderProfile.videoFrameHeight);
    mCamera.setParameters(parameters);

    try {
      mCamera.setPreviewTexture(surfaceTexture);
      mCamera.startPreview();
      new VideoMediaPrepareTask().execute(null, null, null);
    } catch (IOException e) {
      debugLog("Surface texture is unavailable or unsuitable" + e.getMessage());
    }

    debugLog("Exiting initializeCameraView()");
  }

  public static void releaseCamera() {
    if (mCamera != null) {
      System.out.println("camera not null, camera released");
      mCamera.stopPreview();
      // release the camera for other applications
      mCamera.release();
      mCamera = null;
    }
  }

  public boolean prepareRecording() {
    debugLog("Entering prepareRecording()");
    if (mVideoRecorder == null && mCamera == null) return false;

    mVideoRecorder = new MediaRecorder();
    mCamera.unlock();
    mVideoRecorder.setCamera(mCamera);

    mVideoRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
    mVideoRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

    // Customise your profile based on a pre-existing profile
    mVideoRecorder.setProfile(mCamcorderProfile);
    mVideoRecorder.setOrientationHint(90);

    mOutputFile = getOutputVideoMessageFile(this, null);

    if (!surfaceView.isAvailable()) return false;

    if (mOutputFile == null) return false;

    debugLog("mOutputFile::path:" + mOutputFile.getPath());
    mVideoRecorder.setOutputFile(mOutputFile.getPath());

    try {
      mVideoRecorder.prepare();
    } catch (IllegalStateException e) {
      debugLog("IllegalStateException preparing MediaRecorder: " + e.getMessage());
      releaseRecorder();
      return false;
    } catch (IOException e) {
      debugLog("IOException preparing MediaRecorder: " + e.getMessage());
      releaseRecorder();
      return false;
    }

    debugLog("Exiting prepareRecording()");
    return true;
  }

  public static void releaseRecorder() {
    if (mVideoRecorder != null) {
      System.out.println("video recorder not null, recorder released");
      mVideoRecorder.reset();
      mVideoRecorder.release();
      isRecordingVideo = false;
      isVideoRecorderPrepared = false;
      mVideoRecorder = null;
      if (mCamera != null) mCamera.lock();
    }
  }

  @Override
  public void initializeCameraOnView(SurfaceTexture surfaceTexture) {
    initializeCameraView(getBackFacingCameraInstance(), surfaceTexture);
  }

  @Override
  public void releaseCameraFromView() {
    releaseCamera();
  }

  @SuppressLint("StaticFieldLeak")
  public class VideoMediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      //setSurfaceViewAlpha(0.5f);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
      // isMediaPrepared = prepareRecording();
      return prepareRecording();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
      debugLog("Entering VideoMediaPrepareTask onPostExecute() with result:" + aBoolean);
      isVideoRecorderPrepared = aBoolean;
      if (!aBoolean) {
        releaseRecorder();
      } else {
        if (mVideoRecorder != null && isVideoRecorderPrepared) {
          debugLog("video recording start");
          mVideoRecorder.start();
          isRecordingVideo = true;
        }
      }
      debugLog("Exiting VideoMediaPrepareTask onPostExecute()");
    }
  }

  public static void stopVideoRecording() {
    mVideoRecorder.stop();
    releaseCamera();
    releaseRecorder();
    isRecordingVideo = false;
    isVideoRecorderPrepared = false;

    System.out.println("output file:" +mOutputFile.exists());
    System.out.println("output file path:" +mOutputFile.getPath());
    Intent intent = new Intent();
    Uri uri = Uri.fromFile(mOutputFile);
    intent.setData(uri);
    activity.setResult(RESULT_OK, intent);
    activity.finish();
  }

  public static File getOutputVideoMessageFile(Context context, String fileName) {
    ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
    File directory = contextWrapper.getDir("Koya", Context.MODE_PRIVATE);
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());

    File mediaFile;
    //mediaFile = fileName != null ? new File(directory.getPath(), fileName)
    //    : new File(directory.getPath(), File.separator + "VID_" + timeStamp + "" + ".mp4");
    mediaFile = new File(Environment
        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath()
            + "/videocapture_example.mp4");
    return mediaFile;
  }

  public static Camera.Size getOptimalVideoSize(List<Camera.Size> supportedVideoSizes,
      List<Camera.Size> previewSizes, int textureViewWidth, int textureViewHeight) {

    final double ASPECT_TOLERANCE = 0.2;
    double targetRatio = (double) textureViewWidth / textureViewHeight;

    List<Camera.Size> videoSizes;
    if (supportedVideoSizes != null) {
      videoSizes = supportedVideoSizes;
    } else {
      videoSizes = previewSizes;
    }
    Camera.Size optimalSize = null;

    double minDiff = Double.MAX_VALUE;

    int targetHeight = textureViewHeight;

    for (Camera.Size size : videoSizes) {
      double ratio = (double) size.width / size.height;
      if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
      if (Math.abs(size.height - targetHeight) < minDiff && previewSizes.contains(size)) {
        optimalSize = size;
        minDiff = Math.abs(size.height - targetHeight);
      }
    }

    if (optimalSize == null) {
      minDiff = Double.MAX_VALUE;
      for (Camera.Size size : videoSizes) {
        if (Math.abs(size.height - targetHeight) < minDiff && previewSizes.contains(size)) {
          optimalSize = size;
          minDiff = Math.abs(size.height - targetHeight);
          break;
        }
      }
    }

    return optimalSize;
  }

  public static Camera getBackFacingCameraInstance() {
    return getDefaultCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
  }

  private static Camera getDefaultCamera(int cameraId) {
    int mCameraCount = Camera.getNumberOfCameras();

    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

    for (int i = 0; i < mCameraCount; i++) {
      Camera.getCameraInfo(i, cameraInfo);

      if (cameraInfo.facing == cameraId) {
        Camera camera = Camera.open(i);
        camera.setDisplayOrientation(90);
        return camera;
      }
    }

    return null;
  }

  public static boolean hasPermissions(Context context, String... permissions) {
    if (context != null && permissions != null) {
      for (String permission : permissions) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    switch (requestCode) {

      case PERMISSION_ALL:

        if (grantResults.length > 0) {

          boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
          boolean RecordAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
          boolean SendSMSPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

          if (CameraPermission && RecordAudioPermission && SendSMSPermission) {

            Toast.makeText(RecorderActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
            initiateView();
          }
          else {
            Toast.makeText(RecorderActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();

          }
        }

        break;
    }
  }
}
