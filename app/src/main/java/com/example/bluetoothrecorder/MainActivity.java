package com.example.bluetoothrecorder;

import android.content.Intent;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.bluetoothrecorder.java_codegeeks_approach.ChatActivity;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ((Button) findViewById(R.id.btn_codegeeks_chat)).setOnClickListener(v ->
        startActivity(new Intent(this, ChatActivity.class)));
  }
}
