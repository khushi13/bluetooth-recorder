package com.example.bluetoothrecorder;

import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

  String TAG = BaseActivity.class.getSimpleName();

  public abstract String className();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    TAG = className();
  }

  public void debugLog(String message) {
    Log.d(TAG, message);
  }
}
